﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TestWebApplication.Models;


namespace TestWebApplication.Controllers
{
	public class HomeController : Controller
	{

		private readonly ILogger<HomeController> _logger;
		private TestDBContext dB;
		IHubContext<ChatHub> hubContext;
		public HomeController(ILogger<HomeController> logger, TestDBContext context, IHubContext<ChatHub> hub)
		{
			_logger = logger;
			dB = context;
			hubContext = hub;
		}

		private ChatViewModel GetChatModel()
		{
			List<UserModel> userModels = dB.UserApps
			.Select(u => new UserModel { UserID = u.UserID, UserName = u.UserName })
			.ToList();
			List<Message> messages = dB.Messages.ToList();

			return new ChatViewModel { UserAppss = userModels, Messages = messages };
		}
		

		public IActionResult Index()
		{
			return View(dB.UserApps.ToList());
		}

		public IActionResult GetUsers(UserApp userApp)
		{
			dB.UserApps.Add(userApp);
			dB.SaveChanges();
			return PartialView(dB.UserApps.ToList());
		}
		public IActionResult Message()
		{
			EmailMessage receiveMessage = new EmailMessage();
			return View(receiveMessage.ReceiveEmail());
		}

		public IActionResult Chat()
		{ 
            return View(GetChatModel());
		}
		
		[HttpPost]
		public async Task GetMessage(Message messagefromchat)
		{
			dB.Messages.Add(messagefromchat);
			dB.SaveChanges();
			await hubContext.Clients.All.SendAsync("ReceiveMessage", messagefromchat);
		}
		


		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
		
	}
}
