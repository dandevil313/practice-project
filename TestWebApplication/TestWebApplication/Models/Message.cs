﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace TestWebApplication.Models
{
	public class Message
	{

		public string TextOfMessage{ get; set; }

		public string TimeOfMessage { get; set; }

		[Key]
		public int MessageID { get; set; }
		
		public int UserID { get; set; }

		 public UserApp? User { get; set; } 

	}
}
