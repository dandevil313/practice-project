﻿using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWebApplication.Models
{
	public class EmailMessage
	{

		public string subject { get; set; }
		public string content { get; set; }
		public string textFromTxt { get; set; }

		public void Send(EmailMessage emailMessage)
		{
			var message = new MimeMessage();
			message.To.Add(new MailboxAddress("", "dandevil313@mail.com"));
			message.From.Add(new MailboxAddress("Практика", "testuseru5er@yandex.ru"));

			message.Subject = emailMessage.subject; 
			message.Body = new TextPart(TextFormat.Plain)
			{
				Text = emailMessage.content
			};

			using (var client = new SmtpClient())
			{
				client.Connect("smtp.yandex.ru", 25, false);
				
				client.AuthenticationMechanisms.Remove("XOAUTH2");

				client.Authenticate("testuseru5er@yandex.ru", "swbxntpkichurdci");

				client.Send(message);

				client.Disconnect(true);
			}
		}

		public List<EmailMessage> ReceiveEmail(int maxCount = 10)
		{
			using (var emailClient = new Pop3Client())
			{
				emailClient.Connect("pop.yandex.ru", 995, true);

				emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

				emailClient.Authenticate("testuseru5er@yandex.ru", "swbxntpkichurdci");

				List<EmailMessage> emails = new List<EmailMessage>();
		
				for (int i = 0; i < emailClient.Count && i < maxCount; i++)
				{

					var message = emailClient.GetMessage(i);

					var emailMessage = new EmailMessage
					{
						content = message.TextBody,
						subject = message.Subject
					};

					foreach (var txtattachment in message.Attachments)
					{
						if (txtattachment is MimePart)
						{
							emailMessage.textFromTxt = ((TextPart)txtattachment).Text;	
						}
					}
				
					emails.Add(emailMessage);

				}

				return emails;
			}
		}
		
	}
}
