﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TestWebApplication.Models
{
	public class UserApp 
	{
		[Key]
		public int UserID { get; set; }

		public string UserName { get; set; }

		public string Login { get; set; }

		public string Password { get; set; }

		public List<Message> Messages { get; set; } 

	}
}
