﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWebApplication.Models
{
    public class TestDBContext : DbContext
    {

        public DbSet<UserApp> UserApps { get; set; }
        public DbSet<Message> Messages { get; set; }

        public TestDBContext(DbContextOptions<TestDBContext> options)
           : base(options)
        {
            Database.EnsureCreated();
        }

    }
}
